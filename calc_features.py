#!/usr/bin/env python
# coding: utf-8


import talib
import pandas as pd


datafile = 'D:/desktop/forum/回归数据.csv'


def lam(df):
    df['ma5'] = talib.MA(df['Adjprcnd'].values, timeperiod = 5, matype = 0)
    df['ma10'] = talib.MA(df['Adjprcnd'].values, timeperiod = 10, matype = 0)
    df['ma20'] = talib.MA(df['Adjprcnd'].values, timeperiod = 20, matype = 0)
    df['ma30'] = talib.MA(df['Adjprcnd'].values, timeperiod = 30, matype = 0)
    df['ma50'] = talib.MA(df['Adjprcnd'].values, timeperiod = 50, matype = 0)
    df['amt5'] = talib.MA(df['Dnvaltrd'].values, timeperiod = 5, matype = 0)
    df['amt10'] = talib.MA(df['Dnvaltrd'].values, timeperiod = 10, matype = 0)
    df['amt20'] = talib.MA(df['Dnvaltrd'].values, timeperiod = 20, matype = 0)
    df['amt30'] = talib.MA(df['Dnvaltrd'].values, timeperiod = 30, matype = 0)
    df['amt50'] = talib.MA(df['Dnvaltrd'].values, timeperiod = 50, matype = 0)
    df['midprice'] = talib.MIDPOINT(df['Adjprcnd'].values)
    df['atr'] = talib.ATR(df['Hiprc'].values, df['Loprc'].values, df['Clsprc'].values, timeperiod=14)
    df['ad'] = talib.AD(df['Hiprc'].values, df['Loprc'].values, df['Clsprc'].values, df['Dnshrtrd'].values)
    df['ht'] = talib.HT_DCPERIOD(df['Adjprcnd'].values)
    df['adxr'] = talib.ADXR(df['Hiprc'].values, df['Loprc'].values, df['Clsprc'].values, timeperiod=14)
    df['mfi'] = talib.MFI(df['Hiprc'].values, df['Loprc'].values, df['Clsprc'].values, df['Dnshrtrd'].values, timeperiod=14)
    df['pat1'] = talib.CDL3BLACKCROWS(df['Opnprc'].values, df['Hiprc'].values, df['Loprc'].values, df['Clsprc'].values)
    df['pat2'] = talib.CDLBREAKAWAY(df['Opnprc'].values, df['Hiprc'].values, df['Loprc'].values, df['Clsprc'].values)
    df['pat1'] = (df['pat1'] != 0).astype(int)
    df['pat2'] = (df['pat2'] != 0).astype(int)
    return df


if __name__ == '__main__':
    df = pd.read_csv(datafile, dtype={"Stkcd": str, 'Dnshrtrd': float }, parse_dates=['Trddt'])
    df = df.groupby('Stkcd').apply(lam)
    df.to_csv('D:/desktop/forum/5days_dummy.csv', index=False)
    df = df.drop('y', axis=1).rename({'futret': 'y'}, axis=1)
    df.to_csv('D:/desktop/forum/5days_ret.csv', index=False)
