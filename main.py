#!/usr/bin/env python
# coding: utf-8

"""
THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
"""


import pandas as pd
import numpy as np
import os
from sklearn.preprocessing import MinMaxScaler
import torch
import torch.nn as nn
from torch.nn import TransformerEncoder, TransformerEncoderLayer
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader
import matplotlib.pyplot as plt
import math


look_back = 21
SEQ_LEN = 5
EPOCH = 100
LR = 0.0001
train_test_pct = 0.6
szTrain, szTest = 1000, 125
splitvld = int(0.1 * szTrain)
rootdir = '/home/outsider/quant'
features = 'Opnprc,Hiprc,Loprc,Clsprc,Dnshrtrd,Dnvaltrd,Dsmvosd,Dretnd,Adjprcnd,Ret,PE,PB,PCF,PS,Turnover,Liquidility,ma5,ma10,ma20,ma30,ma50,amt5,amt10,amt20,amt30,amt50,midprice,atr,ad,ht,adxr,mfi,pat1,pat2'.split(',')
output = ['y', ]
nfeatures = len(features) * look_back
device = torch.device("cpu")
# 设置随机种子
torch.manual_seed(0)


def save_npy(dataloc, outdir):
    df = pd.read_csv(dataloc, dtype={"Stkcd": str }, parse_dates=['Trddt'])
    df = df.fillna(method='pad').fillna(0)

    for k, v in df.groupby('Stkcd'):
        # 观测数不够
        if v.shape[0] < 3 * look_back:
            print(k)
            continue
        # 分配内存
        out = np.zeros((v.shape[0] - look_back, nfeatures+1))
        date = v['Trddt'][look_back:].values
        arr = v[features].values
        # 对每一天，取过去look_back天的数据（包括当天），将features合并到一起，然后保存对应的y
        for i in range(look_back, v.shape[0]):
            out[i-look_back, :nfeatures] = arr[i-look_back: i, :len(features)].ravel()
        out[:, nfeatures]  = v[output][look_back:].values[:, 0]
        np.savez(os.path.join(outdir, f'{k}'), data=out, date=date)


def load_by_stock(findloc):
    for root, mid, fs in os.walk(findloc):
        for f in fs:
            yield f, np.load(os.path.join(findloc, f))


def scale(train, vld, test):
    scaler = MinMaxScaler()
    x_train = np.c_[train[:, 0], scaler.fit_transform(train[:, 1:-1])]
    x_vld  = np.c_[vld[:, 0], scaler.transform(vld[:, 1:-1])]
    x_test  = np.c_[test[:, 0], scaler.transform(test[:, 1:-1])]
    return [x_train, train[:, -1]], [x_vld, vld[:, -1]], [x_test, test[:, -1]]


class Model_LSTM(nn.Module):
    def __init__(self):
        super(Model_LSTM, self).__init__()
        
        self.lstm = nn.LSTM(
            input_size=nfeatures,   # 输入尺寸为 1，表示一天的数据
            hidden_size=128,
            num_layers=2, 
            batch_first=True)
        
        self.out = nn.Linear(128, 1)
        
    def forward(self, x):
        r_out, (h_n, h_c) = self.lstm(x, None)   # None 表示 hidden state 会用全 0 的 state
        out = self.out(r_out[:, -1:, :])          # 取最后一天作为输出
        return out.flatten()
    
    def __str__(self):
        return 'Model_LSTM'

    @staticmethod
    def name():
        return "MODEL_LSTM"
    
    
def train(rnn, saveasfn, train_loader, vld_loader):
    optimizer = torch.optim.AdamW(rnn.parameters(), lr=LR)  # optimize all cnn parameters
    # 对于AdmaW优化器，CyclicLR要设定参数cycle_momentum=False
    # https://blog.csdn.net/zisuina_2/article/details/103236864
    scheduler = torch.optim.lr_scheduler.CyclicLR(optimizer, 1e-4, 5e-4, step_size_up=500, step_size_down=500, cycle_momentum=False)
    min_trainloss = 10000

    for step in range(EPOCH):
        train_loss = []
        rnn.train()
        for tx, ty in train_loader:
            if torch.cuda.is_available():
                tx = tx.cuda()
                ty = ty.cuda() 
            output = rnn(tx[:, :, 1:])  # 去掉date列
            loss = loss_func(output, ty)        
            optimizer.zero_grad()  # clear gradients for this training step
            loss.backward()  # back propagation, compute gradients
            optimizer.step()
            train_loss.append(loss.cpu().item())

        if sum(train_loss) < min_trainloss:
            min_trainloss = sum(train_loss)
            torch.save(rnn.state_dict(), saveasfn)

        with torch.no_grad():
            vld_loss = []
            rnn.eval()
            for tx, ty in vld_loader:
                if torch.cuda.is_available():
                    tx = tx.cuda()
                    ty = ty.cuda() 

                output = rnn(tx[:, :, 1:])             
                loss = loss_func(output, ty)
                vld_loss.append(loss.cpu().item())
        
        scheduler.step()
    # 不能直接return rnn.load_state_dict(torch.load(saveasfn))
    # 这个函数会直接修改rnn，但是这个函数的返回值是奇怪的东西
    rnn.load_state_dict(torch.load(saveasfn))    
    return rnn
    
    
def test(rnn, test_loader):
    with torch.no_grad():
        # test_loss = []
        out = []
        lidt = []
        rnn.eval()
        for tx, ty in test_loader:
            if torch.cuda.is_available():
                tx = tx.cuda()
                ty = ty.cuda() 
            # print(tx[0, 0, :])
            lidt.append(tx[:, -1, 0].reshape(-1, 1))
            output = rnn(tx[:, :, 1:])  
            out.append( output )
            # loss = loss_func(output, ty)
            # test_loss.append(loss.cpu().item())
#         print('test_loss : %.4f' % sum(test_loss))
        return torch.cat((torch.cat(lidt, 0), torch.cat(out, 0).reshape(-1, 1)), 1)
    

# attention layer code inspired from: https://discuss.pytorch.org/t/self-attention-on-words-and-masking/5671/4
class Attention(nn.Module):
    def __init__(self, hidden_size, batch_first=False):
        super(Attention, self).__init__()

        self.hidden_size = hidden_size
        self.batch_first = batch_first

        self.att_weights = nn.Parameter(torch.Tensor(1, hidden_size), requires_grad=True)

        stdv = 1.0 / np.sqrt(self.hidden_size)
        for weight in self.att_weights:
            nn.init.uniform_(weight, -stdv, stdv)

    def get_mask(self):
        pass

    def forward(self, inputs, lengths):
        if self.batch_first:
            batch_size, max_len = inputs.size()[:2]
        else:
            max_len, batch_size = inputs.size()[:2]
            
        # apply attention layer
        weights = torch.bmm(inputs,
                            self.att_weights  # (1, hidden_size)
                            .permute(1, 0)  # (hidden_size, 1)
                            .unsqueeze(0)  # (1, hidden_size, 1)
                            .repeat(batch_size, 1, 1) # (batch_size, hidden_size, 1)
                            )
    
        attentions = torch.softmax(F.relu(weights.squeeze()), dim=-1)

        # create mask based on the sentence lengths
        mask = torch.ones(attentions.size(), requires_grad=True)

        # apply mask and renormalize attention scores (weights)
        masked = attentions * mask
        _sums = masked.sum(-1).unsqueeze(-1)  # sums per row
        
        attentions = masked.div(_sums)

        # apply attention weights
        weighted = torch.mul(inputs, attentions.unsqueeze(-1).expand_as(inputs))

        # get the final fixed vector representations of the sentences
        representations = weighted.sum(1).squeeze()

        return representations, attentions


# define our own model which is an lstm followed by two dense layers
class LSTM_ATT(nn.Module):
    def __init__(self, hidden_dim=128, lstm_layer=2, dropout=0.2):
        super(LSTM_ATT, self).__init__()
        self.dropout = nn.Dropout(p=dropout)
        self.hidden_size = hidden_dim

        self.lstm1 = nn.LSTM(input_size=nfeatures,
                            hidden_size=hidden_dim,
                            num_layers=1, batch_first=True,
                            bidirectional=True)
        self.atten1 = Attention(hidden_dim*2, batch_first=True) # 2 is bidrectional
        self.lstm2 = nn.LSTM(input_size=hidden_dim*2,
                            hidden_size=hidden_dim,
                            num_layers=1, batch_first=True,
                            bidirectional=True)
        self.atten2 = Attention(hidden_dim*2, batch_first=True)
        self.fc1 = nn.Sequential(nn.Linear(hidden_dim*lstm_layer*2, hidden_dim*lstm_layer*2),
                                 nn.BatchNorm1d(hidden_dim*lstm_layer*2),
                                 nn.ReLU()) 
        self.fc2 = nn.Linear(hidden_dim*lstm_layer*2, 1)

    
    def forward(self, x):
        x = self.dropout(x)

        out1, (h_n, c_n) = self.lstm1(x)
        x, _ = self.atten1(out1, self.hidden_size) # skip connect

        out2, (h_n, c_n) = self.lstm2(out1)

        y, _ = self.atten2(out2, self.hidden_size)
        z = torch.cat([x.reshape(-1, 2*self.hidden_size), y.reshape(-1, 2*self.hidden_size)], dim=1)
        z = self.fc1(self.dropout(z))
        z = self.fc2(self.dropout(z))
        return z.flatten()
    
    def __str__(self):
        return "LSTM_ATT"

    @staticmethod
    def name():
        return "LSTM_ATT"


class TransformerModel(nn.Module):

    def __init__(self, d_model: int=nfeatures, nhead: int=2, d_hid: int=128,
                 nlayers: int=2, dropout: float = 0.5):
        super().__init__()
        self.model_type = 'Transformer'
        self.pos_encoder = PositionalEncoding(d_model, dropout)
        encoder_layers = TransformerEncoderLayer(d_model, nhead, d_hid, dropout, batch_first=True)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.d_model = d_model
        self.decoder = nn.Linear(d_model, 1)

        self.init_weights()

    def init_weights(self) -> None:
        initrange = 0.1
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, src: torch.Tensor) -> torch.Tensor:
        """
        Args:
            src: Tensor, shape [seq_len, batch_size]
            src_mask: Tensor, shape [seq_len, seq_len]

        Returns:
            output Tensor of shape [seq_len, batch_size, ntoken]
        """
        src = self.pos_encoder(src * math.sqrt(self.d_model))
        output = self.transformer_encoder(src, None)
        output = self.decoder(output)
        return output[:, -1, :].flatten()
    
    def __str__(self):
        return "Transformer_Model"

    @staticmethod
    def name():
        return "Transformer_Model"


class PositionalEncoding(nn.Module):

    def __init__(self, d_model: int, dropout: float = 0.1, max_len: int = 5000):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)

        position = torch.arange(max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(d_model) * (-math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, 1, d_model)
        pe[:, 0, :] = torch.sin(position * div_term)
        self.register_buffer('pe', pe)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Args:
            x: Tensor, shape [seq_len, batch_size, embedding_dim]
        """
        x = x + self.pe[:x.size(0)]
        return self.dropout(x)


class LKJY_Model1(nn.Module):
    def __init__(self):
        super().__init__()
        self.lstm = nn.LSTM(
            input_size=nfeatures,   # 输入尺寸为 1，表示一天的数据
            hidden_size=128,
            num_layers=2, 
            batch_first=True)
        self.caan = CAANModel(128)
        
    def forward(self, x):
        r_out, (h_n, h_c) = self.lstm(x, None)   # None 表示 hidden state 会用全 0 的 state
        out = self.caan(r_out)
        return out.flatten()
    
    def __str__(self):
        return "LKJY_Model1"

    @staticmethod
    def name():
        return "LKJY_Model1"

    
class LKJY_Model2(nn.Module):
    def __init__(self, d_model=nfeatures, nhead=2, d_hid=128, dropout=0.5, nlayers=2):
        super().__init__()
        self.model_type = 'Transformer'
        self.pos_encoder = PositionalEncoding(d_model, dropout)
        encoder_layers = TransformerEncoderLayer(d_model, nhead, d_hid, dropout, batch_first=True)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.d_model = d_model
        self.caan = CAANModel(d_model)

    def forward(self, src: torch.Tensor) -> torch.Tensor:
        src = self.pos_encoder(src)
        output = self.transformer_encoder(src, None)
        output = self.caan(output)
        return output.flatten()
        
    def __str__(self):
        return "LKJY_Model2"

    @staticmethod
    def name():
        return "LKJY_Model2"
    

class CAANModel(nn.Module):

    def __init__(self, d_model: int):
        super().__init__()
        self.encoder = nn.MultiheadAttention(d_model, 1, batch_first=True)
        self.decoder = nn.Linear(d_model, 1)


    def forward(self, src: torch.Tensor) -> torch.Tensor:
        """
        Args:
            src: Tensor, shape [seq_len, batch_size]
            src_mask: Tensor, shape [seq_len, seq_len]

        Returns:
            output Tensor of shape [seq_len, batch_size, ntoken]
        """
#         src = self.encoder(src) * math.sqrt(self.d_model)
        out, _ = self.encoder(src, src, src)
        output = self.decoder(out)
        return output[:, -1, :]


# In[84]:


class dSet(object):
    def __init__(self, data, label, seqlen=SEQ_LEN):
        self.data = torch.tensor(data).to(torch.float32)
        self.label = torch.tensor(label).to(torch.float32)
        self.seqlen = seqlen
    
    def __len__(self):
        return len(self.label)
    
    def __getitem__(self, idx):
        if idx < self.seqlen:
            # print(idx, self.seqlen)
            item = torch.zeros((self.seqlen, nfeatures+1))
            item[self.seqlen-idx-1:, :] = self.data[:idx+1, :]
            return item, self.label[idx]
        else:
            return self.data[idx-self.seqlen+1:idx+1, :], self.label[idx]


def divide_ds(arr):
    m = arr.shape[0]
    for splititv in range(szTrain, m-szTest, szTest):
        # 划分训练集、交叉验证和测试集    
        train_arr, vld_arr, test_arr = scale(arr[splititv-szTrain: splititv-splitvld, ...], arr[splititv-splitvld: splititv, ...], arr[splititv:splititv+szTest, ...])
        # 将这些数据集转为torch格式
        # 这里需要加上drop_last=True，防止剩下最后一行batchnorm失败
        # https://www.cnblogs.com/zmbreathing/p/pyTorch_BN_error.html
        train_loader = DataLoader(dSet(*train_arr), batch_size=20, shuffle=False, drop_last=True)
        vld_loader = DataLoader(dSet(*vld_arr), batch_size=20, shuffle=False, drop_last=True)
        test_loader = DataLoader(dSet(*test_arr), batch_size=20, shuffle=False, drop_last=True)
        yield train_loader, vld_loader, test_loader


def get_prediction(mod, npdataloc, modloc, predloc):
    for i, (f, arr0) in enumerate(load_by_stock(npdataloc)):
        if i >= 100:
            break
        stkcd = f.split('.')[0]
        if os.path.exists(os.path.join(predloc, f'{stkcd}.csv')):
            continue
        iddate, uniquedt = pd.factorize(arr0['date'])
        arr = np.c_[iddate, arr0['data']]
        print(f'Model: {mod.name()}, Stkcd: {f}, datalen: {arr.shape[0]}')
        li = []
        rnn = mod().to(device)
        for j, (train_loader, vld_loader, test_loader) in enumerate(divide_ds(arr)):
            # 训练
            rnn = train(rnn, os.path.join(modloc, f'{stkcd}_{j}.pkl'), train_loader, vld_loader)
            # 测试
            li.append(test(rnn, test_loader))
        if len(li) <= 0: continue
        out = torch.cat(li, 0)
        # 测试结果输出
        df = pd.DataFrame({'Trddt': uniquedt[out[:, 0].to(int)], 'pred': out[:, 1]})
        df.to_csv(os.path.join(predloc, f'{stkcd}.csv'), index=False)
        

def collect_pred_result(loadloc):
    # 收集测试结果并合并，准备回测
    dfli = []
    for root, mid, fs in os.walk(loadloc):
        for f in fs:
            stkcd = f.split('.')[0]
            df = pd.read_csv(os.path.join(loadloc, f'{stkcd}.csv'), parse_dates=['Trddt'])
            df['Stkcd'] = stkcd
            dfli.append(df)
    df = pd.concat(dfli)
    return df


def backtest(predloc, testdir, modname):
    pred = collect_pred_result(predloc).sort_values(['Trddt', 'pred'], ascending=[True, False])
    pred = pred.groupby('Trddt').head(10).reset_index().copy()
    pred['pred'] = (pred['pred'] > 0).astype(int)
    pred['ok'] = 1
    baseline = pd.read_csv(f'{rootdir}/回测用数据.csv', dtype={"Stkcd": str }, parse_dates=['Trddt'])
    pred = pred[pred['pred'] == 1].copy()
    df = baseline.merge(pred, on=['Stkcd', 'Trddt'], how='left')
    df['ok'] = df.groupby('Stkcd')['ok'].fillna(method='pad')
    df = df.dropna(subset=['ok',]).fillna(0).drop('ok', axis=1)
    df['sig'] = ((df.groupby('Stkcd')['pred'].shift(1) + df.groupby('Stkcd')['pred'].shift(2) + df.groupby('Stkcd')['pred'].shift(3)) > 0).astype(int)
    df['ret1'] = df['Dretnd'] * df['sig'] + 1
    df['idxret1'] = df['idxret'] + 1
    df['stkret1'] = 1 + df['Dretnd']

    def single_stock(df):
        df['cumstk'] = df.groupby('Stkcd')['stkret1'].cumprod()
        df['cumret'] = df.groupby('Stkcd')['ret1'].cumprod()
        df['cumidx'] = df.groupby('Stkcd')['idxret1'].cumprod()
        df.to_csv(f'{testdir}/backtest.csv', index=False)
        fig, ax = plt.subplots(2, 2)
        c = 0
        n = 0
        for k, v in df.groupby('Stkcd'):
            if not v['sig'].any():
                continue
            v.plot(ax=ax[c // 2, c % 2], title=k, x='Trddt', y=['cumret', 'cumidx', 'cumstk'])
            c += 1
            if c >= 4:
                fig.autofmt_xdate()
                plt.tight_layout()
                plt.savefig(f'{testdir}/backtest-{n}.png')
                plt.close(fig)
                fig, ax = plt.subplots(2, 2)
                c = 0
                n += 1

        if c > 0:
            fig.autofmt_xdate()
            plt.tight_layout()
            plt.savefig(f'{testdir}/backtest-{n}.png')
            plt.close(fig)

    single_stock(df.copy())

    out = df.groupby('Trddt').agg(avgret=('ret1', 'mean'), idxret=('idxret1', 'first')).reset_index()
    out['cumret'] = out['avgret'].cumprod()
    out['cumidx'] = out['idxret'].cumprod()
    print(out.head())
    fig, ax = plt.subplots()
    out.plot(ax=ax, x='Trddt', y=['cumret', 'cumidx'])
    plt.savefig(f'{rootdir}/backtest-{fn}-{modname}.png')
    out.to_csv(f'{rootdir}/backtest-{fn}-{modname}.csv', index=False)


# In[76]:

if __name__ == '__main__':
    fnames = ['5days_dummy', '5days_ret']
    idselected = 0
    fn = fnames[idselected]
    dataloc = f"{rootdir}/{fn}.csv"
    npzdir  = f'{rootdir}/npsave/{fn}'

    if not os.path.exists(npzdir):
        os.makedirs(npzdir)
        save_npy(dataloc, npzdir)

    loss_funcs = [nn.BCEWithLogitsLoss(), nn.MSELoss()]
    loss_func = loss_funcs[idselected]

    mods = [Model_LSTM, LSTM_ATT, TransformerModel, LKJY_Model1, LKJY_Model2]

    for m in mods:
        predloc = f'{rootdir}/pred_res/{fn}/{m.name()}'
        modloc  = f'{rootdir}/model/{fn}/{m.name()}'
        testloc = f'{rootdir}/test/{fn}/{m.name()}'
        if not os.path.exists(predloc):
            os.makedirs(predloc)
        if not os.path.exists(modloc):
            os.makedirs(modloc)
        if not os.path.exists(testloc):
            os.makedirs(testloc)
        get_prediction(m, npzdir, modloc, predloc)
        backtest(predloc, testloc, m.name())
