/*导入数据*/
/*日个股回报率文件*/
%macro dalyr();
%do i = 0 %to 5;
DATA TRD_Dalyr&i (Label="日个股回报率文件");
Infile "D:\Desktop\forum\rawdata\TRD_Dalyr&i..txt" encoding="utf-8" delimiter = '09'x Missover Dsd lrecl=32767 firstobs=2;
Format Stkcd $6. Trddt yymmdd10. Opnprc 9.3 Hiprc 9.3 Loprc 9.3 Clsprc 9.3;
Format Dnshrtrd 12.0 Dnvaltrd 16.3 Dsmvosd 16.2 Dsmvtll 16.2 Dretwd 10.6;
Format Dretnd 10.6 Adjprcwd 14.6 Adjprcnd 14.6 Markettype 10.0 Capchgdt $10.;
Format Trdsta 10.0 Ahshrtrd_D 12.0 Ahvaltrd_D 16.3;
Informat Stkcd $6. Trddt yymmdd10. Opnprc 9.3 Hiprc 9.3 Loprc 9.3 Clsprc 9.3;
Informat Dnshrtrd 12.0 Dnvaltrd 16.3 Dsmvosd 16.2 Dsmvtll 16.2 Dretwd 10.6;
Informat Dretnd 10.6 Adjprcwd 14.6 Adjprcnd 14.6 Markettype 10.0 Capchgdt $10.;
Informat Trdsta 10.0 Ahshrtrd_D 12.0 Ahvaltrd_D 16.3;
Label Stkcd="证券代码" Trddt="交易日期" Opnprc="日开盘价" Hiprc="日最高价" Loprc="日最低价";
Label Clsprc="日收盘价" Dnshrtrd="日个股交易股数" Dnvaltrd="日个股交易金额" Dsmvosd="日个股流通市值";
Label Dsmvtll="日个股总市值" Dretwd="考虑现金红利再投资的日个股回报率" Dretnd="不考虑现金红利的日个股回报率";
Label Adjprcwd="考虑现金红利再投资的收盘价的可比价格" Adjprcnd="不考虑现金红利的收盘价的可比价格";
Label Markettype="市场类型" Capchgdt="最新股本变动日期" Trdsta="交易状态";
Label Ahshrtrd_D="日盘后成交总量" Ahvaltrd_D="日盘后成交总额";
Input Stkcd $ Trddt Opnprc Hiprc Loprc Clsprc Dnshrtrd Dnvaltrd Dsmvosd Dsmvtll Dretwd Dretnd Adjprcwd Adjprcnd Markettype Capchgdt $ Trdsta Ahshrtrd_D Ahvaltrd_D;
Run;
%end;
data data.daily; set %do i = 0 %to 5; TRD_Dalyr&i %end; ; 
by Stkcd Trddt; run;
%mend dalyr;
%dalyr();

/*个股日交易衍生指标*/
%macro import_deri();
%do i = 0 %to 5;
DATA STK_MKT_DALYR&i (Label="个股日交易衍生指标");
Infile "D:\Desktop\forum\rawdata\STK_MKT_DALYR&i..txt" encoding="utf-8" delimiter = '09'x Missover Dsd lrecl=32767 firstobs=2;
Format SecurityID 20.0 TradingDate yymmdd10. Symbol $6. ShortName $10. Ret 20.6;
Format PE 20.6 PB 20.6 PCF 20.6 PS 20.6 Turnover 10.5 CirculatedMarketValue 20.2;
Format ChangeRatio 10.5 Amount 20.0 Liquidility 20.6;
Informat SecurityID 20.0 TradingDate yymmdd10. Symbol $6. ShortName $10. Ret 20.6;
Informat PE 20.6 PB 20.6 PCF 20.6 PS 20.6 Turnover 10.5 CirculatedMarketValue 20.2;
Informat ChangeRatio 10.5 Amount 20.0 Liquidility 20.6;
Label SecurityID="证券ID" TradingDate="交易日期" Symbol="证券代码" ShortName="股票简称";
Label Ret="股息率(股票获利率)(%)" PE="市盈率" PB="市净率" PCF="市现率" PS="市销率";
Label Turnover="换手率(%)" CirculatedMarketValue="流通市值" ChangeRatio="涨跌幅";
Label Amount="成交金额" Liquidility="流动性指标";
Input SecurityID TradingDate Symbol $ ShortName $ Ret PE PB PCF PS Turnover CirculatedMarketValue ChangeRatio Amount Liquidility;
Run;
%end;
data data.indicator_day;
set %do i = 0 %to 5; STK_MKT_DALYR&i %end; ;
by Symbol TradingDate; run;
%mend import_deri;
%import_deri();

/*数据处理、合并*/
data day5; merge data.daily data.indicator_day(rename=(symbol=stkcd TradingDate=trddt));
by stkcd trddt;
drop Trdsta Ahshrtrd_D Ahvaltrd_D Capchgdt Markettype Adjprcwd Dretwd Dsmvtll SecurityID ShortName ChangeRatio CirculatedMarketValue Amount;
run;
proc expand data=day5 out=outreg method=none;
by stkcd;
convert dretnd    = futret        / tout = (+1 movprod 5 -1 lead 5);
run;
proc sort data=outreg; by stkcd trddt; run;

/*导入沪深300成分股*/
data hs300;
infile 'D:/desktop/forum/沪深300.csv' dlm=',' firstobs=1 dsd missover lrecl=32767;
informat stkcd $6.;
input stkcd $;
ok = 1; run;
/*仅保留沪深300的股票*/
data outreg1; set outreg;
if _n_ = 1 then do;
  declare hash h(dataset: 'hs300');
  h.definekey('stkcd');
  h.definedata('ok');
  h.definedone();
  call missing(ok);
end;
rc = h.find();
y=futret > 0.03;
if rc = 0;
Opnprc = Opnprc * Adjprcnd / Clsprc;
Hiprc  = Hiprc  * Adjprcnd / Clsprc;
Loprc  = Loprc  * Adjprcnd / Clsprc;
Clsprc = Clsprc * Adjprcnd / Clsprc;
drop rc ok time;
run;

proc export data=outreg1
outfile='D:\Desktop\forum\回归数据.csv'
dbms=csv replace; run;
